package food;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Food {

    public final Lock lock = new ReentrantLock();
    private int units;
    private String name;

    public Food(int units, String name){
        this.units = units;
        this.name = name;
    }

    public int getUnits(){
        return this.units;
    }

    public String getName(){
        return this.name;
    }

    public void decrementOneUnit(){
        this.units--;
    }

}
