package cell;

import space.GameSpace;

import java.util.Timer;
import java.util.TimerTask;

public abstract class Cell implements Runnable{

    private String name;
    private boolean alive;
    private boolean full;

    private static int T_FULL = 10;
    private static int T_HUNGRY = 10;
    private int counter; //we use the same counter for T_FULL and T_HUNGRY;

    private Timer timer = new Timer();

    private int eatenFood;

    public static GameSpace gameSpace;

    public Cell(String name){
        this.name = name;
        this.alive = true;
        this.full = false;
        this.counter = T_HUNGRY;
        this.eatenFood = 0;

    }

    public void startTimer(){
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(counter > 0) {
                    counter--;
                } else {
                    if(full){
                        counter = T_HUNGRY;
                        full = false;
                    } else {
                        alive = false;
                        //1...5 units of food
                    }
                }
            }
        }, 0, 1000);
    }

    public void eat(){
        if(gameSpace.checkSpaceForFood(this)){
            System.out.println("Cell " + this.getName() + " ate.");
            eatenFood++;
            this.full = true;
            this.counter = T_FULL;
            startTimer();
        }

    }

    public boolean canReproduce(){
        return this.eatenFood >= 10 && this.full;
    }

    public abstract void reproduce(); //this method will be implemented in each subclass: SexualCell and AsexualCell

    public String getName(){
        return this.name;
    }

    public void run(){

    }

    public void addCellToSpace(Cell cell){


    }
}
