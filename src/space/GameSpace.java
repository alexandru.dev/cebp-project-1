package space;

import cell.Cell;
import food.Food;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GameSpace {

    private static int MAX_NUMBER_OF_CELLS = 10;
    private static int MAX_NUMBER_OF_FOOD_UNITS = 10;

    private int countCells = 0;
    private int countFood = 0;

    private ArrayList<Cell> cells = new ArrayList<>();
    private ArrayList<Food> food = new ArrayList<>();
    private static GameSpace singleton = null;

    private Lock lock = new ReentrantLock();

    public static GameSpace getGameSpace(){
        if(singleton == null)
            singleton = new GameSpace();
        return singleton;
    }

    public void addCell(Cell cell){
        if(countCells < MAX_NUMBER_OF_CELLS){
            cells.add(cell);
            countCells++;
        }
    }

    public void addFood(Food resource){
        if (countFood < MAX_NUMBER_OF_FOOD_UNITS) {
            food.add(resource);
            countFood++;
        }
    }

    public boolean checkSpaceForFood(Cell cell){
        boolean lockedFood;
        for(Food resource: food){
            lockedFood = resource.lock.tryLock();
            if(lockedFood) {
                try {
                    System.out.println(cell.getName() + " locked a food unit.");
                    if (resource.getUnits() > 0) {
                        resource.decrementOneUnit();
                        System.out.println("There are " + resource.getUnits() + " units left of " + resource.getName());
                        return true;
                    }
                } finally {
                    System.out.println(cell.getName() + " unlocked a food unit");
                    resource.lock.unlock(); //to avoid a deadlock
                }
            } else {
                System.out.println(cell.getName() + " tried to lock a food unit.");
            }
        }
        return false;
    }

 }
